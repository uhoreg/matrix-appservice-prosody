Matrix-XMPP bridge proof-of-concept
===================================

Prosody module to act as a Matrix Application Service.  This module allows XMPP
users to join Matrix rooms as if they were MUC rooms, unlike most Matrix
bridges that allow Matrix users to join rooms from other networks.  I did this
direction since it seemed easier, and I was mostly interested in seeing how
well the idea worked.

This module is a modified version of the muc plugin from Prosody.  Only the
`muc.lua` (renamed to `mod_muc_matrix.lua`) and `muc.lib.lua` (renamed to
`muc_matrix.lib.lua`) files have been modified, and `matrix.lib.lua` has been
added.  You can view a diff of the latest commit and the first
commit
(https://gitlab.com/uhoreg/matrix-appservice-prosody/commit/e74e9f2e357c52397d69c24fee629538af70b805)
of this branch to see what has been changed.

*Warning:* This is a very, very, very badly written proof-of-concept.  Don't
even think of using it as a starting point for anything (with the possible
exception of `matrix.lib.lua`).  Do not let this code near anything that is
production-level.  It is just an investigation into how Prosody could be
adapted into a Matrix Application Service.

Configuration
-------------

```
Component "xmppmucserver" "muc_matrix";
  name = "Experimental Matrix bridge";
  domain = "matrixserver";
  prefix = "_xmpp_";
  sender_localpart = "xmpp";
  homeserver = "http://127.0.0.1:8008";
  as_token = "secret1";
  hs_token = "secret2";
  user_jid_domain = "xmppserver";
```

Usage
-----

To join a Matrix room with your XMPP user, percent-escape the full room alias
or ID (including the leading "#" or "!"), and use that as the MUC room ID, at
the configured MUC server.  Note that MUC room IDs will be lower-cased, so any
upper-case letters will need to be escaped.  The plugin only bridges plain text
messages, and joins/leaves from the XMPP side.

How to make a proper bridge using Prosody
-----------------------------------------

### General

* decide on a mapping from Matrix user IDs to
  JIDs. e.g. `@localpart:matrixserver` ->
  `escaped_localpart%3Amatrixserver@xmppserver`
  - may want to define a shorter version for local users.
    e.g. `@localpart:matrixserver` -> `escaped_localpart@xmppserver`
* decide on a mapping from JIDs to Matrix user IDs. e.g. `localpart@xmppserver`
  -> `@_xmpp_escaped_localpart=40xmppserver:matrixserver`

*Note:* beware of size limitations on IDs (both MXIDs and JIDs).

### XMPP users joining Matrix rooms

* familiarize yourself with [XEP-0045](https://xmpp.org/extensions/xep-0045.html)

* decide on a mapping from Matrix room aliases/ids to JIDs.  e.g.

    - by alias: `#localpart:matrixserver` ->
      `%23escaped_localpart%3Aserver@xmppmucserver`
    - by ID: `!id` -> `%21escaped_id@xmppmucserver` (note that XMPP downcases
      IDs, so any upper-case letters in the room ID will need to be escaped)

    may want to define a shortcut for local aliases (e.g. so that XMPP users can
    join `#matrix:matrix.org` by joining `matrix@conference.matrix.org` instead
    of `%23matrix%3Amatrix.org@conference.matrix.org`)

* figure out how to handle multiple names on the XMPP side for the same Matrix room
  - redirect users to a canonical address (using the room ID) using the
    "alternate venue" feature?  (How well do clients implement that?)
    - allow people to join any of the XMPP rooms, and just mirror the content?
      (make sure that you send messages to XMPP users from the right room(s))

* figure out how to name resources (nicknames) for Matrix users on the XMPP
  side. e.g. "displayname (@mxid)".
  - make sure that XMPP users are not allowed to conflict with Matrix users

* figure out how to deal with semantic differences, such as
  - multiple bare JIDs can join the same MUC as different nicks -- probably
    need to limit a bare JID to one nick

* modify mod_muc to talk to Matrix server
  - define configuration variables (the ones that I've defined, see above, are
    probably fine)
  - insert HTTP calls to Matrix server at the appropriate points (e.g. hooks)
    - this will involve a lot of refactoring to do properly because Prosody
      assumes most hooks in mod_muc are synchronous.  It will definitely need
      a lot more than what I've done.
    - a lot of stanzas should be round-tripped through the Matrix server,
      rather than having Prosody route things internally (which may actually
      make some logic easier).  For example, see how
      `room_mt:handle_groupchat_to_room` sends the message to the Matrix
      server, and the call to `room:broadcast_message` now happens when the
      message comes back from the Matrix server in the `PUT /transactions/*`
      route instead of in `handle_groupchat_to_room`.
  - index XMPP rooms by Matrix room ID
  - index room occupands by JID
  - add HTTP server to listen for events from Matrix server (for this use case,
    it's mainly the `PUT /transactions/*` route)
    - the HTTP server will need to talk to both the modified muc plugin and the
      mod_matrix plugin (described below), so need to figure out how to do
      that.  Possibly with a [shared table](http://prosody.im/doc/developers/moduleapi#moduleshared)?
  - query HS directory to return a list of rooms for disco queries?

... etc.

### Matrix users joining XMPP MUCs - portal

* decide on a mapping from JIDs to Matrix room aliases,
  e.g. `roomname@conference.somexmppserver` ->
  `#_xmpp_escapedroomname=40conference.somexmppserver@matrixserver`

* read https://matrix.org/blog/2017/03/14/an-adventure-in-irc-land/ and see
  what in there applies to this case

* create a `mod_matrix` to handle this case -- this plugin will create XMPP
  accounts for Matrix accounts

...

TODO

### Plumbing Matrix rooms with XMPP MUCs

TODO

### XMPP users starting chats with Matrix users

TODO

### Matrix users starting chats with XMPP users

TODO
