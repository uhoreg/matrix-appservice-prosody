-- Matrix library
-- by 2017 Hubert Chathi

-- This is free and unencumbered software released into the public domain.

-- Anyone is free to copy, modify, publish, use, compile, sell, or
-- distribute this software, either in source code form or as a compiled
-- binary, for any purpose, commercial or non-commercial, and by any
-- means.

-- In jurisdictions that recognize copyright laws, the author or authors
-- of this software dedicate any and all copyright interest in the
-- software to the public domain. We make this dedication for the benefit
-- of the public at large and to the detriment of our heirs and
-- successors. We intend this dedication to be an overt act of
-- relinquishment in perpetuity of all present and future rights to this
-- software under copyright law.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
-- OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
-- ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
-- OTHER DEALINGS IN THE SOFTWARE.

-- For more information, please refer to <http://unlicense.org/>

local matrix_domain = module:get_option_string("domain");
local matrix_prefix = module:get_option_string("prefix", "_xmpp_");
local sender_localpart = module:get_option_string("sender_localpart");
local homeserver = module:get_option_string("homeserver");
local as_token = module:get_option_string("as_token");
local hs_token = module:get_option_string("hs_token");
local user_jid_domain = module:get_option_string("user_jid_domain");

local http = require "net.http";
local json = require "util.json";
local jid_split = require "util.jid".split;
local jid_node = require "util.jid".node;
local jid_host = require "util.jid".host;

local _M = {};

function jid_to_mxid(jid)
	local node, server = jid_split(jid)
	-- FIXME: escape node and server
	return "@" .. matrix_prefix .. node .. "=40" .. server .. ":" .. matrix_domain;
end

_M.jid_to_mxid = jid_to_mxid;

function jid_to_localpart(jid)
	local node, server = jid_split(jid)
	-- FIXME: escape node and server
	return matrix_prefix .. node .. "=40" .. server;
end

_M.jid_to_localpart = jid_to_localpart;

function mxid_to_jid(mxid)
	if not mxid then return; end
	-- check if the MXID belongs to a bridged XMPP user
	-- if so, then return their native JID
	-- FIXME: escape matrix_prefix and matrix_domain
	local node, xserver = string.match(mxid, "^@" .. matrix_prefix .. "([^:]+)=40([^:]+):" .. matrix_domain .. "$");
	if node and xserver then
		-- FIXME: unescape and re-escape node and xserver
		return node .. "@" .. xserver;
	end

	local localpart, localpartend = string.match(mxid, "^@([^:]+):()");
	local server = string.match(mxid, "^(.+)$", localpartend);
	-- FIXME: escape localpart and server
	return localpart .. "%3A" .. server .. "@" .. user_jid_domain;
end

_M.mxid_to_jid = mxid_to_jid;

function is_matrix_user(jid)
	return jid_host(jid) == user_jid_domain;
end

_M.is_matrix_user = is_matrix_user;

function make_url(endpoint, jid, queryparams)
	local url = homeserver .. "/_matrix/" .. endpoint .. "?access_token=" .. as_token;
	if jid then
		url = url .. "&user_id=" .. http.urlencode(jid_to_mxid(jid));
	end
	if queryparams then
		url = url .. "&" .. queryparams
	end
	return url;
end

_M.make_url = make_url;

function request(method, endpoint, jid, queryparams, headers, body, callback)
	local new_headers = headers or {};
	if body and type(body) == "table" then
		new_headers.content_type = "application/json";
		body = json.encode(body);
	end
	local data = {
		method = method;
		headers = headers;
		body = body;
	}
	return http.request(make_url(endpoint, jid, queryparams), data, callback);
end

_M.request = request;

return _M;
